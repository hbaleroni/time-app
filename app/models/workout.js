/**
 * Created by hbaleroni on 6/12/14.
 */

/**
 * Module dependencies
 */
var mongoose = require('mongoose');
var random = require('mongoose-random');
var Schema = mongoose.Schema;

/**
 * Workout Schema
 */
var WorkoutSchema;
WorkoutSchema = new Schema({
    name: String,
    detail: {
        description: String,
        goal: String
    },
    steps: [
        {
            order: Number,
            parts: [
                {
                    exerciseType: String,
                    inputValue: String
                }
            ]
        }
    ]
});

mongoose.model('workout', WorkoutSchema);