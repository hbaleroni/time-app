/**
 * Created by hbaleroni on 6/13/14.
 */

/**
 * Module dependencies
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

/**
 * Result Schema
 */
var ResultSchema;
ResultSchema = new Schema({
    user_id: String,
    workout_id: String,
    amountAccomplished: Number,
    datetime: Date,
    steps: [
        {
            parts: [
                {
                    inputValue: Number
                }
            ]
        }
    ]
});

mongoose.model('result', ResultSchema);