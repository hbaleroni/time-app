/**
 * Created by hbaleroni on 7/7/14.
 */

/**
 * Module dependencies
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

/**
 *  User Schema
 */
var UserSchema;
UserSchema = new Schema({
    user_id: String,
    name: String,
    email: String,
    password: String,
    weight: Number
});

mongoose.model('user', UserSchema);