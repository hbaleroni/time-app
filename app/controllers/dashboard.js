var mongoose = require('mongoose');
var WorkoutModel = mongoose.model('workout', WorkoutModel);

exports.dashboard = function(req, res){
    var data = {};

    data.pageTitle = 'Dashboard';
    data.workout_of_the_day = {};

    // Finding a random workout of the day
//    data.query = WorkoutModel.where({'name': 'Angie'});
//
////    data.query.findOne(function(err, workout){
////        if(err){
////            res.render('err', data);
////        }
////
//////        console.log(workout);
////    });

    res.render('dashboard', data);

};