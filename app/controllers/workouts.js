var mongoose = require('mongoose');
var WorkoutModel = mongoose.model('workout', WorkoutModel);

exports.readAll = function(req, res){

    var data = {};
    data.pageTitle = 'Workouts';

    WorkoutModel.find({}, function(err, results){

        if(err){
            res.render('err', data);
        }

        data.workouts = results;

        res.render('workouts', data);

    });
};

exports.readById = function(req, res) {

    var data = {};
    data.workoutId = req.params._id;

    WorkoutModel.findById(data.workoutId, function(err, callback){
        if(err){
            res.render('err', data);
        }

        if(callback){
            data.workout = callback;
            data.pageTitle = callback.name;

            res.render('workout', data);
        }
    });
};