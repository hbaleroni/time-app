var mongoose = require('mongoose');
var ResultModel = mongoose.model('result', ResultModel);

exports.readAll = function(req, res){

    var data = {};
    data.pageTitle = 'Results';

    ResultModel.find({}, function(err, results){

        if(err){
            res.render('err', data);
        }

        data.workouts = results;

        res.render('results', data);

    });
};

exports.readOne = function(req, res){
    var data = {};
    // req.workoutName is coming from the .jade input field
    ResultModel.findOne({'name': req.workoutName}, function(err, result){
        if(err){
            res.render('err', data);
        }
        data.workout = result;

        res.render('result', data);

    });
};

exports.saveResult = function(req,res) {
    var data = req.body;
    data.workoutId = req.body.workoutId;
    data.workout = req.body.workoutData;

    //I have no idea how to make this data save properly into the DB
    //the entire workout DB has different structure for each workout
    //I can't figure out a way to save them correctly
    var saveData = new ResultModel({
        user_id: "",
        workout_id: data.workoutId,
        amountAccomplished: data.workout.goal.value,
        datetime: new Date(),
        steps: [
            {
                parts: [
                    {
                        inputValue: data.workout.exercise.name[0]
                    },
                    {
                        exerciseType: data.workout.type.typ[0],
                        inputValue: data.workout.amount.value[0]
                    }
                ]
            },
            {
                parts: [
                    {
                        inputValue: data.workout.exercise.name[1]
                    },
                    {
                        exerciseType: data.workout.type.typ[1],
                        inputValue: data.workout.amount.value[1]
                    },
                    {
                        exerciseType: data.workout.type.typ[2],
                        inputValue: data.workout.amount.value[2]
                    }
                ]
            },
            {
                // Example of how messed up my data actually is
                // I can't figure out a way to make this controller
                // fit all of the workout types.
                parts: [
                    {
                        inputValue: data.workout.exercise.name[2]
                    },
                    {
                        exerciseType: data.workout.type.typ[3],
                        inputValue: data.workout.amount.value[3]
                    },
                    {
                        exerciseType: data.workout.type.typ[2],
                        inputValue: data.workout.amount.value[2]
                    }
                ]
            }

        ]

    }); // end of saveData

    ResultModel.update(data.workoutId, saveData, function(err){
        if(err){
            console.log('ERROR: '+ err);
        }
    });

    // Shows the data coming from the form as an obj
    // Testing
    console.log('workoutId: '+data.workoutId);
    console.log(data.workout);

    // Redirecting to Dashboard
    res.redirect('/');
};