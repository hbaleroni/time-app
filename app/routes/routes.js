/**
 * Created by hbaleroni on 6/19/14.
 */

var workoutsController = require('../controllers/workouts');
var dashboardController = require('../controllers/dashboard');
var resultsController = require('../controllers/results');
var passport = require('passport');

module.exports = function(app) {

    // GET
    // ROUTE to the landing page
    app.get('/', function(req, res){
        // Checking for user auth
        res.render('../views/index.jade');
    });

    // ROUTE for the dashboard page, user logged in
    app.get('/dashboard', ensureAuthenticated, dashboardController.dashboard);

    // ROUTE for the workouts page, user logged in
    app.get('/workouts', ensureAuthenticated, workoutsController.readAll);

    // ROUTE for the individual workout page, user logged in
    app.get('/workouts/:_id', ensureAuthenticated, workoutsController.readById);

    // ROUTE Results
    app.get('/results', ensureAuthenticated, resultsController.readAll);

    app.post('/save_result', ensureAuthenticated, resultsController.saveResult);

    // Redirect the user to Facebook for authentication.  When complete,
    // Facebook will redirect the user back to the application at
    //     /auth/facebook/callback
    app.get('/auth/facebook', passport.authenticate('facebook'));

    // Facebook will redirect the user to this URL after approval.  Finish the
    // authentication process by attempting to obtain an access token.  If
    // access was granted, the user will be logged in.  Otherwise,
    // authentication has failed.
    app.get('/auth/facebook/callback',
        passport.authenticate('facebook',{
            successRedirect: '/dashboard',
            failureRedirect: '/login'
    }));

    // LOGOUT
    app.get('/logout', function(req, res){
        req.logout();
        res.redirect('/');
    });

    // test authentication for routing authenticates users
    function ensureAuthenticated(req, res, next) {
        if (req.isAuthenticated()) { return next(); }
        res.redirect('/')
    }

};