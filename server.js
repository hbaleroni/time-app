/**
 * Created by hbaleroni on 6/19/14.
 */

// requires statements
var express = require('express');
var mongoose = require('mongoose');
var http = require('http');
var path = require('path');
var bodyParser = require('body-parser');
var passport = require('passport');
var FacebookStrategy = require('passport-facebook').Strategy;
var app = express();

// DB config file
var db = require('./config/db');
mongoose.connect('mongodb://localhost/TimeDB');

// CONFIG
app.configure(function() {
    var oneDay = 86400000;
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({"extended":true}));
    app.use(express.compress());
    app.use(express.static(__dirname + '/style', { maxAge: oneDay }));
    app.use(express.session({ secret: 'handbanana' }));
    app.use(passport.initialize());
    app.use(passport.session());
    app.use(app.router);
});

// Configuration file for express
app.set('views', path.join(__dirname, '/app/views'));
app.set('view engine', 'jade');

require('./app/models/result.js');
require('./app/models/workout.js');
require('./app/models/user.js');
require('./app/routes/routes.js')(app);

User = mongoose.model('user');

// PASSPORT Login stuff
// serialize and deserialize
passport.serializeUser(function(user, done) {
    console.log('serializeUser: ' + user._id);
    done(null, user._id);
});
passport.deserializeUser(function(id, done) {
    User.findById(id, function(err, user){
        if(!err) done(null, user);
        else done(err, null)
    })
});

// LOGIN INFO
passport.use(new FacebookStrategy({
        clientID: '307993636035999',
        clientSecret: '2da97cd2de68e9fa1054af9365e06660',
        callbackURL: 'http://localhost:3000/auth/facebook/callback'
    },
    function(accessToken, refreshToken, profile, done) {
        User.findOne({ user_id: profile.id }, function(err, user) {
            if(err) { console.log(err); }
            if (!err && user != null) {
                done(null, user);
            } else {
                var user = new User({
                    user_id: profile.id,
                    name: profile.displayName,
                    created: Date.now()
                });
                user.save(function(err) {
                    if(err) {
                        console.log(err);
                    } else {
                        console.log("saving user ...");
                        done(null, user);
                    }
                });
            }
        });
    }
));

var server = app.listen(3000, function() {
    console.log('Listening on port %d', server.address().port);
});

exports = module.exports = app;